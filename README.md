# Chat Thingy

Chat thingy is a MUC-client based on [stanza.io](https://github.com/otalk/stanza.io), [Semantic UI](http://semantic-ui.com/) and [AngularJS](https://angularjs.org/).

## Install

copy config.js.dist to config.js and edit the file.
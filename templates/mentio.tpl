<div class="ui segment" id="nickselector">
    <div class="ui list">
        <div class="item" mentio-menu-item="item" ng-repeat="item in items track by $index">
            <img class="ui avatar image" data-ng-src="{{ item.avatar }}"/>

            <div class="content">
                <a class="header" ng-bind-html="item.label | mentioHighlight:typedTerm:'menu-highlighted' | unsafe">Daniel Louise</a>
            </div>
        </div>
    </div>
</div>
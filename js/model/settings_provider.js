var SettingStorageProvider = function () {
    this.type = 'localStorage';
    if (!localStorage['settings']) {
        localStorage['settings'] = {};
    }
}

SettingStorageProvider.prototype.get = function (key) {
    if (localStorage[key]) {
        try {
            return JSON.parse(localStorage[key]);
        } catch (e) {
            return null;
        }
    } else {
        return null;
    }
};

SettingStorageProvider.prototype.set = function (key, value) {
    if (value == null) {
        delete localStorage[key];
    } else {
        localStorage[key] = JSON.stringify(value);
    }
};

//# sourceURL=/js/model/settings_provider.js
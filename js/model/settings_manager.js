var SettingsManager = function () {
    this.settings = [];
    this.groups = [];
    this.storage_provider = new SettingStorageProvider();

    var setting_group = new SettingGroup();
    setting_group.key = 'chat';
    setting_group.display_name = 'Chat';

    this.groups.push(setting_group);

    setting_group = new SettingGroup();
    setting_group.key = 'notifications';
    setting_group.display_name = 'Notifications';

    this.groups.push(setting_group);

    var setting = new Setting('chat.default_nickname', '', 'text', '', this.storage_provider);
    setting.display_name = 'Default nickname';
    setting.setting_group = this.getGroupByName('chat');
    setting.description = 'The default nickname shown in the login form.';
    this.settings.push(setting);

    setting = new Setting('notifications.show_text', false, 'boolean', false, this.storage_provider);
    setting.display_name = 'Messages in notifications';
    setting.setting_group = this.getGroupByName('notifications');
    setting.description = 'If you get a bubble notification, show the full message.';
    this.settings.push(setting);

    setting = new Setting('notifications.color', '#FFDCDC', 'color', '#FFDCDC', this.storage_provider);
    setting.display_name = 'Highlight Color';
    setting.setting_group = this.getGroupByName('notifications');
    setting.description = 'The color highlighted messages should have.';
    setting.change = function (color) {
        jss.remove('#messagestream .highlighted.message.event');
        jss.set('#messagestream .highlighted.message.event', {
            'background-color': color.getValue()
        });
    };
    setting.change(setting);
    this.settings.push(setting);

    this.loadValuesFromStorage();

};

SettingsManager.prototype.loadValuesFromStorage = function () {
    var self = this;
    this.settings.forEach(function (el) {
        var setting = self.storage_provider.get(el.key);
        if (setting) {
            el.value = setting;
        }
    });
};

SettingsManager.prototype.getGroupByName = function (name) {
    var group = null;
    this.groups.forEach(function (el) {
        if (el.key == name) {
            group = el;
        }
    });
    return group;
};

SettingsManager.prototype.getValue = function (key) {
    var setting = null;
    this.settings.forEach(function (el) {
        if (el.key == key) {
            setting = el;
        }
    });
    return (setting.value ? setting.value : setting.default_value);
};

//# sourceURL=/js/model/settings_manager.js
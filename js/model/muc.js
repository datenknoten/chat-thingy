var MUC = function (jid) {
    this.jid = jid;
    this.subject = 'Chat';
    this.participants = [];
    this.scope = null;
    this.client = null;
};

MUC.prototype.sortParticipants = function (a, b) {
    if (a.nickname < b.nickname)
        return -1;
    if (a.nickname > b.nickname)
        return 1;
    return 0;
}

MUC.prototype.setScope = function (scope) {
    this.scope = scope;
};

MUC.prototype.addParticipant = function (stanza) {
    var exists = false;
    var loc_stanza = stanza;
    var participant = this.getParticipant(loc_stanza.from.full);

    if (!participant) {
        participant = new User(loc_stanza.from.full);
        participant.setScope(this.scope);
        participant.parsePresence(loc_stanza);
        this.participants.push(participant);
        participant.getAvatar();
    } else {
        participant.parsePresence(loc_stanza);
        participant.getAvatar();
    }
    this.participants.sort(this.sortParticipants);
};

MUC.prototype.getParticipant = function (jid) {
    var retval = null;
    this.participants.forEach(function (user) {
        if (jid == user.jid) {
            retval = user;
        }
    });
    return retval;
};

MUC.prototype.parseMessage = function (stanza) {
    var message = {
        type: 'message',
        user: this.getParticipant(stanza.from.full),
        date: (stanza.delay ? stanza.delay.stamp : new Date()),
        message: stanza.body
    };
    if (!message.user) {
        // we got a message from an offline user
        var user = new User(stanza.from.full);
        user.nickname = stanza.from.resource;
        user.setScope(this.scope);
        message.user = user;
        this.participants.push(user);
        this.participants.sort(this.sortParticipants);
    }

    var $scope = angular.element($('#messagestream')[0]).scope();

    var regex = new RegExp($scope.service.me.nickname.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'));
    if (stanza.body.match(regex)) {
        message.message = message.message.replace(regex, '#@$&@#');
        message.type = 'highlighted message';
        if ($scope.service.settings.getValue('notifications.show_text')) {
            $scope.service.notifications.addNotification(this.jid, stanza.body, 1);
        } else {
            $scope.service.notifications.addNotification(this.jid, message.user.nickname + ' mentioned you', 1);
        }
    } else {
        $scope.service.notifications.addNotification('New Message', '');
    }

    var last_message = $scope.service.messagestream.pop();
    if (last_message && (last_message.user.jid == message.user.jid) && (last_message.type == 'message')) {
        last_message.message = last_message.message + "\n" + message.message;
        last_message.type = message.type;
        $scope.service.messagestream.push(last_message);
    } else {
        $scope.service.messagestream.push(last_message);
        $scope.service.messagestream.push(message);
    }
};

//# sourceURL=/js/model/muc.js

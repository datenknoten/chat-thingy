var SettingGroup = function () {
    this.key = '';
    this.display_name = '';
};

var Setting = function (key, value, type, default_value, storage_provider) {
    this.key = key;
    this.value = value;
    this.type = type;
    this.default_value = default_value;
    this.display_name = key;
    this.setting_group = new SettingGroup();
    this.description = '';
    this.storage_provider = storage_provider;
    this.change = null;
    var self = this;
    watch(this, "value", function () {
        if (self.change) {
            self.change(self);
        }
    });
    watch(this, "value", function () {
        if (self.storage_provider) {
            if (self.value == self.default_value) {
                self.storage_provider.set(self.key, null);
            } else {
                self.storage_provider.set(self.key, self.value);
            }
        }
    });
};

Setting.prototype.getValue = function () {
    return (this.value ? this.value : this.default_value);
};


//# sourceURL=/js/model/settings.js

var User = function (jid) {
    this.jid = jid;
    this.nickname = '';
    this.avatar = ravatar(jid);
    this.hash = '';
    this.scope = null;
    this.status = 'offline';
    this.status_message = '';
    this.type = 'unavailable';
    this.label = '';
    var self = this;
    watch(this, "nickname", function () {
        self.label = self.nickname;
    });
};

User.prototype.setScope = function (scope) {
    this.scope = scope;
};

User.prototype.getAvatar = function () {
    var user = this;
    this.scope.service.client.getVCard(this.jid, function (err, data) {
        if (!err) {
            if (data.vCardTemp.photo) {
                var uri = 'data:' + data.vCardTemp.photo.type + ';base64,' + data.vCardTemp.photo.data;
                user.scope.$apply(function () {
                    user.avatar = uri;
                });
            }
        }
    });
};

User.prototype.parsePresence = function (stanza) {
    var message = {
        type: 'presence',
        user: this,
        date: (stanza.delay ? stanza.delay.stamp : new Date()),
        message: ''
    };
    if (stanza.muc && stanza.muc.codes && (stanza.muc.codes.indexOf('303') >= 0)) {
        // user renamed so only update the user object
        message.oldnick = this.nickname;
        this.nickname = stanza.muc.nick;
        this.jid = stanza.from.bare + '/' + this.nickname;
        message.action = 'rename';
        message.message = this.status_message;
    } else if (stanza.type == 'unavailable') {
        // user leaves the muc
        this.status = 'offline';
        if (stanza.status) {
            this.status_message = stanza.status;
        } else {
            this.status_message = '';
        }
        this.type = stanza.type;
        message.action = 'leave';
        message.message = this.status_message;
    } else {
        this.type = stanza.type;
        var old_status = this.status;
        this.nickname = stanza.from.resource;
        if (stanza.show)
            this.status = stanza.show;
        else
            this.status = 'online';
        if (stanza.status) {
            this.status_message = stanza.status;
        }

        if (old_status.indexOf('offline') == -1) {
            message = null;
        } else {
            message.action = 'join';
            message.message = this.status_message;
        }
    }
    if (message) {
        this.scope.service.messagestream.push(message);
    }
};

//# sourceURL=/js/model/user.js

if (config.debug.saveErrors) {
    // try to setup the error logger as early as possible
    window.onerror = function (errorMsg, url, lineNumber) {
        jQuery.post('log.php', {
            'errorMsg': errorMsg,
            'url': url,
            'lineNumber': lineNumber
        });

        return false;
    }
}

function logger(name, data) {
    console.log([name, data]);
}

function handleErrors(data) {
    console.log(data);
    if (data.error.code == "409") {
        // Nickname is already taken
        $('.field.nickname').addClass('error');
        $('.field.nickname .pointing.label').show();
        config.client.disconnect();
    }
}

function handleInput(e) {
    var $input = $('#input').find('textarea');
    if ((e.keyCode == 13) && e.shiftKey) {
        e.shiftKey = false;
    } else if (e.keyCode == 13) {
        var body = filterXSS($input.val());
        var message = {
            to: config.muc,
            type: 'groupchat',
            body: body,
            requestReceipt: true
        };

        config.client.sendMessage(message);

        $input.val('');
        $input.focus();

        return false;
    }
    return true;
}

function sizeTextInput(e) {
    if ((e.keyCode == 13) || (e.keyCode == 8)) {
        var $input = $('#input').find('textarea');
        var lines = $input.val().split("\n").length;
        $input.css('height', "calc(0.78571em + 0.78571em + " + lines * 3 + "ex)");
        while ($input[0].clientHeight < $input[0].scrollHeight) {
            $input.css('height', "calc(0.78571em + 0.78571em + " + lines * 3 + "ex)");
            lines = lines + 1;
        }
    }
}

function renderMessage(data) {
    if (data.body) {
        var $scope = angular.element($('#messagestream')[0]).scope();
        $scope.$apply(function () {
            $scope.service.muc.parseMessage(data);
        });
    }
}

function renderContact(data) {
    var $scope = angular.element($('#messagestream')[0]).scope();
    if (data.to.full == '' || $scope.nickname == data.from.resource) {
        return false;
    }
    $scope.$apply(function () {
        $scope.service.muc.addParticipant(data);
    });
}

$(document).ready(function () {
    $('#topbar').find('.tooltip')
        .popup({
            'position': 'bottom right'
        });
    $('#participantlist')
        .sidebar('setting', 'transition', 'overlay')
        .sidebar('setting', 'mobileTransition', 'overlay')
        .sidebar('attach events', '#participantlist-button');

    $('#informationbar')
        .sidebar('setting', 'transition', 'overlay')
        .sidebar('setting', 'mobileTransition', 'overlay')
        .sidebar('attach events', '#informationbar-button');

    $('#settings')
        .modal('attach events', '#settings-button', 'show');


    $('#smilie-button')
        .popup({
            position: 'top right',
            on: 'click',
            popup: $('#smilie-selector')
        })
    ;

    $('#smilie-selector').find('.button').on('click', function (e) {
        var $textarea = $('#input').find('textarea');
        var caretPos = $textarea[0].selectionStart;
        var textAreaTxt = $textarea.val();
        var txtToAdd = $(e.currentTarget).find('.emoji').text();
        $textarea.val(textAreaTxt.substring(0, caretPos) + txtToAdd + textAreaTxt.substring(caretPos));
    });

    $('body').removeClass('nojs');

    jQuery.getJSON('bower.json', function (data) {
        $('#chat-thingy-version').text("Version: " + data.version);
    });
});

(function (angular) {
    'use strict';
    angular.module('chatthingy', ['luegg.directives', 'ngSanitize', 'angularMoment', 'mentio'])
        .filter('formatOutput', function () {
            return function (input) {
                var output = input
                    .replace(/</g, "&lt;")
                    .replace(/>/g, "&gt;")
                    .replace(/#@/g, '<span class="highlight-text">')
                    .replace(/@#/g, '</span>');
                output = Autolinker.link(output, {
                    'twitter': false,
                    replaceFn: function (autolinker, match) {
                        switch (match.getType()) {
                            case 'url' :
                                var tag = autolinker.getTagBuilder().build(match);
                                if (match.getUrl().indexOf('https://www.krautspace.de') === 0) {
                                    tag.addClass('kraut');
                                    return tag;
                                } else if (match.getUrl().indexOf('https') === 0) {
                                    tag.addClass('icon').addClass('secure');
                                    return tag;
                                } else if (match.getUrl().indexOf('http') === 0) {
                                    tag.addClass('icon').addClass('insecure');
                                    return tag;
                                } else {
                                    return true;  // let Autolinker perform its normal anchor tag replacement
                                }
                        }
                    }
                });
                output = output.replace(/\n/g, '<br />');
                return output;
            };
        })
        .factory('XMPPService', function () {
            var xmppservice = function () {
                this.client = null;
                this.me = null;
                this.muc = null;
                this.notifications = new NotificationManager();
                this.settings = new SettingsManager();
                this.messagestream = [];
            };
            return new xmppservice();
        })
        .controller('MucController', ['$scope', 'XMPPService', function ($scope, XMPPService) {
            $scope.text = '';
            XMPPService.muc = new MUC(config.muc);
            XMPPService.muc.setScope($scope);
            $scope.service = XMPPService;
        }])
        .controller('SettingsController', ['$scope', 'XMPPService', function ($scope, XMPPService) {
            $scope.service = XMPPService;
            $scope.group = 'chat';
            $scope.changeGroup = function (item) {
                $scope.group = item.key;
            };

            $scope.bool_values = [{
                label: 'Yes',
                value: true
            }, {
                label: 'No',
                value: false
            }];

            $scope.search = function (setting) {
                if (setting.setting_group.key == $scope.group) {
                    return true;
                } else {
                    return false;
                }
            };

            $scope.initializeSemantic = function () {
                $('select').dropdown();
            };
        }])
        .controller('LoginController', ['$scope', 'XMPPService', function ($scope, XMPPService) {
            $scope.service = XMPPService;

            $scope.nickname = $scope.service.settings.getValue('chat.default_nickname');

            var getMucScope = function () {
                var scope = angular.element($('#messagestream')[0]).scope();
                return scope;
            };
            var signoutClick = function () {
                XMPPService.client.disconnect();
                return false;
            };
            var showLoginForm = function () {
                getMucScope().$apply(function () {
                    $scope.service.muc = new MUC(config.muc);
                    $scope.service.muc.setScope($scope);
                    $scope.service.client = null;
                });
                $('#messagestream').hide();
                $('#input')
                    .hide()
                    .unbind('keypress');
                $(window).unbind('resize');
                $('#infobutton,#signout').addClass('disabled');
                $('#signout').unbind('click');
                $('#input textarea').unbind('keypress');
                $('#input textarea').unbind('keyup');
                $('#loginform').show();
            };
            var setupSession = function (data) {
                XMPPService.me = new User(data.full);
                XMPPService.me.nickname = $scope.nickname;
                XMPPService.client.getRoster(function (err, resp) {
                    XMPPService.client.updateCaps();
                    XMPPService.client.sendPresence({
                        caps: XMPPService.client.disco.caps
                    });
                    XMPPService.client.joinRoom(config.muc, $scope.nickname);
                });
                window.onbeforeunload = function (e) {
                    XMPPService.client.disconnect();
                };
            };
            var sizeMessagestream = function () {
                var $parent_messagestream = $('#messagestream').parent();
                var height = $(window).height() - $('#input').outerHeight(true) - $('#topbar').outerHeight(true) - 30;

                $parent_messagestream.height(height);

                $('#muc-subject').width(Math.round(($(window).width() - $('#topbar .menu.right').width() - ($('#muc-subject').outerWidth(true) - $('#muc-subject').width()))));
            };
            var setupChatInterface = function (data) {
                $('#loginform').hide();
                $('#messagestream').show();
                $('#input').show();
                $('#input textarea').on('keypress', handleInput);
                $('#input textarea').on('keyup', sizeTextInput);
                sizeMessagestream();
                $(window).on('resize', sizeMessagestream);
                $('#infobutton,#signout').removeClass('disabled');
                $('#signout').on('click', signoutClick);
            };
            var changeSubject = function (data) {
                XMPPService.muc.subject = data.subject;
            };

            $scope.login = function () {

                var local_conf = {
                    boshURL: config.bosh,
                    transports: ['bosh'],
                    jid: 'anon@' + config.host,
                    sasl: ['anonymous']
                };

                XMPPService.client = XMPP.createClient(local_conf);

                if (config.debug.showStanzas)
                    XMPPService.client.on('*', logger);

                XMPPService.client.on('session:started', setupSession);
                XMPPService.client.on('muc:join', setupChatInterface);
                XMPPService.client.on('muc:error', handleErrors);
                XMPPService.client.on('muc:subject', changeSubject);
                XMPPService.client.on('message', renderMessage);
                XMPPService.client.on('presence', renderContact);
                XMPPService.client.on('disconnected', showLoginForm);

                XMPPService.client.connect();
                config.client = XMPPService.client;
            };
        }])
        .directive('onFinishRender', ['$timeout', '$parse', function ($timeout, $parse) {
            return {
                restrict: 'A',
                link: function (scope, element, attr) {
                    if (scope.$last === true) {
                        $timeout(function () {
                            scope.$emit('ngRepeatFinished');
                            if (!!attr.onFinishRender) {
                                $parse(attr.onFinishRender)(scope);
                            }
                        });
                    }
                }
            }
        }]);
})(window.angular);

//# sourceURL=/js/chat.js

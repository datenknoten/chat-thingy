var NotificationMessage = function (title, body, priority) {
    this.title = title;
    this.body = body;
    this.priority = (priority ? priority : 0);
};

var NotificationManager = function () {
    this.notifications = [];
    this.favicon = new Favico({
        animation: 'fade'
    });

    Notification.requestPermission();

    var self = this;

    window.setInterval(function () {
        if (document.hasFocus() && (self.notifications.length > 0)) {
            self.clearNotifications();
        }
    }, 500);

    $(document).on('visibilitychange', function (e) {
        if (document.visibilityState == 'visible') {
            self.clearNotifications();
        }
    });
};

NotificationManager.prototype.clearNotifications = function () {
    this.notifications = [];
    this.favicon.reset();
};

NotificationManager.prototype.addNotification = function (title, body, priority) {
    if (!document.hasFocus()) {
        var notif = new NotificationMessage(title, body, priority);
        this.notifications.push(notif);
        if (notif.priority >= 0) {
            this.favicon.badge(this.notifications.length);
            if (notif.priority >= 1) {
                console.log([notif, title, body]);
                var notification = new Notification(title, {
                    'body': body,
                    'icon': 'favicon/favicon.png'
                });
                console.log(notification);
                notification.onshow = function () {
                    // hide the notification after 15s. This should be configureable
                    setTimeout(function () {
                        notification.close();
                    }, 10000);
                }
            }
        }
        return notif;
    }
};

//# sourceURL=/js/notifications.js

if (config) {
    var files = [];
    if (config.debug.loadFullJS) {
        files = [
            'bower_components/semantic-ui/dist/semantic.js',
            'bower_components/moment/moment.js',
            'bower_components/angularjs/angular.js',
            'bower_components/angular-sanitize/angular-sanitize.js',
            'bower_components/angular-scroll-glue/src/scrollglue.js',
            'bower_components/angular-moment/angular-moment.js',
            'bower_components/xss/dist/xss.js',
            'js/custom/mentio.js',
            'bower_components/watch/src/watch.js',
            'bower_components/Autolinker.js/dist/Autolinker.js',
            'bower_components/favico.js/favico.js',
            'bower_components/seedrandom/seedrandom.js',
            'bower_components/jss/jss.js',
            'bower_components/spectrum/spectrum.js',
            'js/stanza.io.js',
            'js/model/settings_provider.js',
            'js/model/settings.js',
            'js/model/settings_manager.js',
            'js/model/user.js',
            'js/model/muc.js',
            'js/helper.js',
            'js/notifications.js',
            'js/chat.js'
        ];
    } else {
        jQuery.ajaxSetup({cache: true});
        files = [
            'js/build.js'
        ];
    }
    files.forEach(function (file) {
        $('<script>')
            .attr('src', file)
            .appendTo('body');
    });
    jQuery.ajaxSetup({cache: false});
}